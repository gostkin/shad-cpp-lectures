#include <iostream>
#include <vector>

int foo(int a, int b) {
    if (a == b) {
        return a;
    } else {
        return a + b;
    }
}

int bar(int n) {
    int sum = 0;
    for (int i = 0; i < n; ++i) {
        sum += i;
    }
    return sum;
}

void PrintEven(const std::vector<int>& data) {
    for (size_t i = 0; i < data.size(); i += 2) {
        std::cout << data[i] << " ";
    }
}

int main() {
    std::cout << foo(5, 3) << "\n";
    std::cout << bar(5) << "\n";
    std::vector<int> v{1, 2, 3};
    PrintEven(v);
    return 0;
}
